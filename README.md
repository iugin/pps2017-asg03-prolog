# Project structure
* The solutions to Lab 10 and Lab 11 can be found in the folder relative to the Prolog language: `src/main/prolog`.
* In the scala folder (`src/main/scala`) can be found the prolog engine.
* In the scala test folder (`src/test/scala`) can be found the testing library (`PrologMatchers.scala`) and an example of use (`Lab10Test.scala`)
