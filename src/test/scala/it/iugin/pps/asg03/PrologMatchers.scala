package it.iugin.pps.asg03

import alice.tuprolog.SolveInfo
import org.scalatest.{Assertion, Matchers}

trait PrologMatchers extends Matchers {
  this: PrologEngine =>

  protected def endlessIterationEmulationValue: Int

  implicit class RichSolutionWithMatchers(solution: Stream[SolveInfo]) {

    def shouldSucceed: Assertion = solution.isSuccessful shouldBe true

    def shouldFail: Assertion = solution.isSuccessful shouldBe false

    def shouldSucceedAtLeast(n: Int): TimeAssertion = TimeAssertion(() => {
      solution hasSuccessfulSolutions n shouldBe true
    })

    def shouldSucceedAtMost(n: Int): TimeAssertion = TimeAssertion(() => {
      solution hasSuccessfulSolutions n + 1 shouldBe false
    })

    def shouldSucceed(n: Int): TimeAssertion = TimeAssertion(() => {
      solution shouldSucceedAtLeast n times;
      solution shouldSucceedAtMost n times
    })

    def shouldSucceedEndless: Assertion =
      solution shouldSucceedAtLeast endlessIterationEmulationValue times

    case class TimeAssertion(strategy: () => Assertion) {
      def times: Assertion = strategy()
    }

  }

}
