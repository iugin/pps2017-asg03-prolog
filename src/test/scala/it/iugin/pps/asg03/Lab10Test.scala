package it.iugin.pps.asg03

import java.io.File

import org.scalatest.FunSpec

class Lab10Test extends FunSpec with PrologEngine with PrologMatchers {

  private val engine = mkPrologEngine(new File("src/main/prolog/Lab10.pl"))

  override protected val endlessIterationEmulationValue: Int = 100

  describe("Exercise 1.1 - search") {

    it("when the element is in list should, succeed") {
      engine("search(a,[a,b,c])") shouldSucceed
    }

    it("when the element is not in the list, should fail") {
      engine("search(a,[c,d,e])") shouldFail
    }

    it("when element as output, iterates over the list") {
      engine("search(X,[a,b,c])") shouldSucceed 3 times
    }

    it("when list with some variable, iterates over the list variables") {
      engine("search(a,[X,b,Y,Z])") shouldSucceed 3 times
    }

    it("when the list is a variable, endless iteration of lists with the searched element") {
      engine("search(a,X)") shouldSucceedEndless
    }
  }
}
