% EX 1.1
% DOC
% search(?Elem,?List)
% 
% TESTS
% search(a,[a,b,c]).		-> Yes
% search(a,[c,d,e]).		-> No
% search(X,[a,b,c]).		-> One solution for each element of the list (a, b, c)
% search(a,X).					-> Endless iteration of lists with a in any position
% search(a,[X,b,Y,Z]).	-> 3 lists: a in any variable position (X, Y or Z)
% search(X,Y).					-> Endless iteration of lists with generic variable X in any position (for every possible value of X)
search(X,[X|Xs]).
search(X,[_|Xs]) :- search(X,Xs).

% EX 1.2
% DOC
% search2(?Elem,?List)
% 
% TESTS
% search2(a,[b,c,a,a,d,e,a,a,g,h]).	-> It finds 2 solutions: positions (3,4) and positions (7,8)
% search2(a,[b,c,a,a,a,d,e]).				-> It finds 2 solutions: positions (3,4) and positions (4,5)
% search2(X,[b,c,a,a,d,d,e]).				-> It finds every couple in the list, there are 2 solutions: a in positions (3,4) and d in position (5,6)
% search2(a,L).											-> Endless iteration of lists containing 2 consecutive a in any position
% search2(a,[_,_,a,_,a,_]).					-> Every possible combination where there are 2 consecutive a (completing the list with variables)
search2(X,[X,X|_]).
search2(X,[_|Xs]) :- search2(X,Xs).

% EX 1.3
% DOC
% search_two(?Elem,?List)
% 
% TESTS
% search_two(a,[b,c,a,a,d,e]).		-> no
% search_two(a,[b,c,a,d,a,d,e]).	-> yes
search_two(X,[X,_,X|_]).
search_two(X,[_|Xs]) :- search_two(X,Xs).

% EX 1.4
% DOC
% search_anytwo(?Elem,?List)
% 
% TESTS
% search_anytwo(a,[b,c,a,a,d,e]).			-> yes
% search_anytwo(a,[b,c,a,d,e,a,d,e]).	-> yes
search_anytwo(H,[H|T]) :- search(H, T).
search_anytwo(X,[_|T]) :- search_anytwo(X,T).

% EX 2.1
% DOC
% size(?List,?Size)
% 
% TESTS
% size([],X).				-> X/0
% size([a,b,c],X).	-> X/3
% size(X,4).				-> Return a list of 4 elements as first result, then it keep executing
%												with the other N values where N+1 is not M; there should be a cut.
size([],0).
size([_|T],M) :- size(T,N), M is N+1.

% EX 2.2
% DOC
% size_p(?List,?Size)
% 
% TESTS
% size_p([],X).					-> X/zero
% size_p([a,b,c],X).		-> X/s(s(s(zero)))
% size_p(X,s(s(zero))).	-> Return a list of 2 elements.
size_p([],zero).
size_p([_|T],s(N)) :- size_p(T,N).

% EX 2.3
% DOC
% sum(+List,-Sum)
% 
% TESTS
% sum([1,2,3],X). -> X/6
% sum(X, 3). -> Error: the predicate is not fully relational
sum([], 0).
sum([H|T], S) :- sum(T,S1), S is S1 + H.

% EX 2.4
% DOC
% average(+List,-Average)
% 
% TESTS
% average([3,4,3],A).			-> A/3
% average([3,4,3],0,0,A).	-> A/3
% average([4,3],1,3,A).		-> A/3
% average([3],2,7,A).			-> A/3
% average([],3,10,A). 		-> A/3
average(List,A) :- average(List,0,0,A).
average([],C,S,A) :- A is S/C.
average([X|Xs],C,S,A) :-
	C2 is C+1,
	S2 is S+X,
	average(Xs,C2,S2,A).

% EX 2.5 (tail)
% DOC
% max(+List,-Max)
% 
% TESTS
% max([1,2,3,4,5], X). -> X/5
% max([5,4,1,6,3,4,1], X). -> X/6
max([H|T], Max) :- max(T, Max, H).
max([], M, M).
max([H|T], M, TM) :- H > TM, max(T, M, H).
max([H|T], M, TM) :- H =< TM, max(T, M, TM).

% EX 2.5 (no-tail)
% DOC
% max_n(+List,-Max)
% 
% TESTS
% max_n([1,2,3,4,5], X). -> X/5
% max_n([5,4,1,6,3,4,1], X). -> X/6
max_n([H], H).
max_n([H|T], H) :- max_n(T, TM), H > TM.
max_n([H|T], TM) :- max_n(T, TM), H =< TM.

% EX 3.1
% DOC
% same(?List1,?List2)
% 
% TESTS
% same([1,2,3],[1,2,3]).		-> Yes
% same([1,2,3],[1,2,4]).		-> No
% same([1,2,3],Y).					-> Y / [1,2,3]
% same(X,[1,2,3]).					-> X / [1,2,3]
% same(X,Y).								-> Endless iteration of all couple of lists X,Y containing the same variables in each respective position
same([],[]).
same([X|Xs],[X|Ys]):- same(Xs,Ys).

% EX 3.2
% DOC
% all_bigger(+List1,+List2)
% 
% TESTS
% all_bigger([10,20,30,40],[9,19,29,39]).		-> Yes
% all_bigger([10,20,30,40],[10,20,30,40]).	-> No
% all_bigger([10,20,30,40],[9,19,29,40]).		-> No
all_bigger([],[]).
all_bigger([X|Xs],[Y|Ys]):- X > Y, all_bigger(Xs,Ys).

% EX 3.3
% DOC
% sublist(?List1,?List2)
% 
% TESTS
% sublist([1,2],[5,3,2,1]).				-> Yes
% sublist([1,1,1,1,2],[5,3,2,1]).	-> Yes
% sublist([1,2,9],[5,3,2,1]).			-> No
% sublist([1,2,9],X).							-> Endless iteration of all the possible lists containing 1,2 and 9 in any position
% sublist(X,[1,2,9]).							-> Endless iteration of all the possible sublists of [1,2,9]
sublist([],_).
sublist([H|T],List) :- search(H,List), sublist(T,List).

% EX 4.1
% DOC
% seq(+N,?List)
% 
% TESTS
% seq(5,[0,0,0,0,0]).			-> Yes
% seq(5,X).								-> X / [0,0,0,0,0]
% seq(X,[0,0]).						-> Halt (not fully relational)
seq(0, []).
seq(N, [0|T]):- N > 0, N2 is N-1, seq(N2,T).

% EX 4.2
% DOC
% seqR(?N,?List)
% 
% TESTS
% seqR(4,[4,3,2,1,0]).		-> Yes
% seqR(4,X).							-> X / [4,3,2,1,0]
% seqR(X,[4,3,2,1,0]).		-> X / 4
seqR(0, [0]).
seqR(N, [N|T]) :- N > 0, N2 is N-1, seqR(N2,T).

% EX 4.3
% DOC
% seqR2(+N,?List)
% 
% TESTS
% seqR2(4,[0,1,2,3,4]).		-> Yes
% seqR2(4,X).							-> X / [0,1,2,3,4]
% seqR2(X,[0,1,2,3,4]).		-> Halt (not fully relational)
last(T, H, L) :- append(T,[H],L).
seqR2(0, [0]).
seqR2(N, L) :- 
	N > 0,
	N2 is N-1,
	seqR2(N2,T),
	last(T, N, L).

% EX OPTIONAL 1
% DOC
% inv(?List1,?List2)
% 
% TESTS
% inv([1,2,3],[3,2,1]).	-> Yes
% inv([1,2,3],[3,2]).		-> No
% inv(X,[3,2,1]).				-> X / [1,2,3]
% inv([1,2,3],Y).				-> Y / [3,2,1]
inv([],[]).
inv([X|Xs],List2) :- append(InvXs,[X],List2), inv(Xs, InvXs).

% EX OPTIONAL 2
% DOC
% double(?List,?List)
% 
% TESTS
% double([1,2,3],[1,2,3,1,2,3]).	-> Yes
% double(X,[1,2,3,1,2,3]).				-> X / [1,2,3]
% double([1,2,3],X).							-> X / [1,2,3,1,2,3]
double(List1,List2) :- append(List1,List1,List2).

% EX OPTIONAL 3
% DOC
% times(+List,+N,?List)
% 
% TESTS
% times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).		-> Yes
% times([1,2,3],4,X).											-> X / [1,2,3,1,2,3,1,2,3,1,2,3]
% times([1,2,3],0,X).											-> X / []
times(List,0,[]).
times(List,N,NList) :- append(List,RecList,NList), N2 is N-1, times(List,N2,RecList).

% EX OPTIONAL 3
% DOC
% proj(?List,?List)
% 
% TESTS
% proj([[1,2],[3,4],[5,6]],[1,3,5]).		-> Yes
% proj([[1,2],[3,4],[5,6]],X).					-> X / [1,3,5]
% proj(Y,[1,3,5]).											-> Y / [[1|_10400068],[3|_10400071],[5|_10400074]]
proj([],[]).
proj([[X|_]|Xs],[X|R]) :- proj(Xs,R).