% Ex 1
% DOC
% dropAny(?Elem,?List,?OutList)
%
% TESTS
% dropAny(10,[10,20,10,30,10],L).			-> L / [20,10,30,10] ; L / [10,20,30,10] ; L / [10,20,10,30]
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]) :- dropAny(X,Xs,L).

% Ex 1.1
% DOC
% dropFirst(?Elem,?List,?OutList)
%
% TESTS
% dropFirst(10,[10,20,10,30,10],L).			-> L / [20,10,30,10]
% dropFirst(10,[20,10,20,10,30,10],L).	-> L / [20,20,10,30,10]
dropFirst(X,[X|T],T) :- !.
dropFirst(X,[H|Xs],[H|L]) :- dropFirst(X,Xs,L).
% DOC
% dropLast(?Elem,?List,?OutList)
%
% TESTS
% dropLast(10,[10,20,10,30,10],L).		-> L / [10,20,10,30]
% dropLast(10,[10,20,10,30,10,30],L).		-> L / [10,20,10,30,30]
dropLast(X,[H|Xs],[H|L]) :- dropLast(X,Xs,L),!.
dropLast(X,[X|T],T).
% DOC
% dropAll(?Elem,?List,?OutList)
%
% TESTS
% dropAll(10,[10,20,10,30,10],L).			-> L / [20,30]
dropAll(X,[],[]).
dropAll(X,[X1|T],L) :- copy_term(X, X1),!,dropAll(X,T,L).
dropAll(X,[H|Xs],[H|L]) :- dropAll(X,Xs,L).

% Ex 2.1
% DOC
% fromList(+List,-Graph)
%
% TESTS
% fromList([10,20,30],[e(10,20),e(20,30)]).		-> Yes
% fromList([10,20],[e(10,20)]).								-> Yes
% fromList([10],[]).													-> Yes
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]) :- fromList([H2|T],L).

% Ex 2.2
% DOC
% fromCircList(+List,-Graph)
%
% TESTS
% fromCircList([10,20,30],[e(10,20),e(20,30),e(30,10)]).	-> Yes
% fromCircList([10,20],[e(10,20),e(20,10)]).							-> Yes
% fromCircList([10],[e(10,10)]).													-> Yes
fromCircList(F,[H],[e(H,F)]).
fromCircList(F,[H1,H2|T],[e(H1,H2)|L]) :- fromCircList(F,[H2|T],L).
fromCircList([H|T],L) :- fromCircList(H,[H|T],L).

% Ex 2.3
% DOC
% dropNode(+Graph, +Node, -OutGraph)
%
% TESTS
% dropNode([e(1,2),e(1,3),e(2,3)],1,[e(2,3)]). -> Yes
dropNode(G,N,O):-
	dropAll(e(N,_),G,G2),
	dropAll(e(_,N),G2,O).

% Ex 2.4
% DOC
% reaching(+Graph, +Node, -List)
%
% TESTS
% reaching([e(1,2),e(1,3),e(2,3)],1,L).	-> L/[2,3]
% reaching([e(1,2),e(1,2),e(2,3)],1,L).	-> L/[2,2]
reaching(G,N,L) :- findall(X,member(e(N,X),G),L).

% Ex 2.5
% DOC
% anypath(+Graph, +Node1, +Node2, -ListPath)
%
% TESTS
% anypath([e(1,2),e(1,3),e(2,3)],1,3,L).	-> L/[e(1,3)] ; L/[e(1,2),e(2,3)]
anypath(G,N1,N2,[e(N1,N2)]) :- member(e(N1,N2),G).
anypath(G,N1,N2,[e(N1,N3)|L]) :- member(e(N1,N3),G), anypath(G,N3,N2,L).

% Ex 2.6
% DOC
% allreaching(+Graph, +Node, -List)
%
% TESTS
% allreaching([e(1,2),e(2,3),e(3,5)],1,[2,3,5]).	-> Yes
% allreaching([e(1,2),e(2,3),e(3,5)],2,[2,3,5]).	-> No (not circular)
allreaching(G,N,[]) :- not(member(e(N,_),G)).
allreaching(G,N,[N2|L]) :- member(e(N,N2),G), allreaching(G,N2,L).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Ex 3: TIC-TAC-TOE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% DOC
% next(@Table,@Player,-Result,-NewTable)
% TESTS
% next([[n,n,n],[o,x,x],[o,x,n]],x,R,G).	-> 4 results, one winning x
next(Table,Player,Result,NewTable) :-
	move(Table,Player,NewTable),
	result(NewTable,Result).

% DOC - places the marker for the Player
% move(+Graph,+Player,-NewGraph
% TESTS
% move([[n,n,n],[o,x,x],[o,x,n]],o,G).	-> 4 possible placing
move([n|R],Player,[Player|R]).									% Place the marker in head if it is empty
move([H|T],Player,[H|L]) :-	move(T,Player,L).		% Recursive navigate to next rows or columns (respectively if H is an element or a list)
move([Row|T], Player, [NewRow|T]) :- is_list(Row), member(n,Row), !, move(Row,Player,NewRow).	% Recursive require the marker in the Row

% DOC - All the possible winning configurations
% winning(+Board,-Result)
% Do NOT use the cut at the end of the winning predicate, otherwise
% it returns only one winner for board, the first in order (never even)
% TESTS
% winning([[n,n,n],[x,o,x],[o,x,n]],nothing).	-> Yes
% winning([[n,n,n],[x,x,x],[o,x,n]],win(x)).		-> Yes
% winning([[n,n,o],[x,o,x],[o,x,n]],win(o)).		-> Yes
% winning([[o,n,n],[o,o,x],[o,x,n]],win(o)).		-> Yes
winning([[P,_,_],[P,_,_],[P,_,_]],win(P)) :- not(P = n).	% First col
winning([[_,P,_],[_,P,_],[_,P,_]],win(P)) :- not(P = n).	% Second col
winning([[_,_,P],[_,_,P],[_,_,P]],win(P)) :- not(P = n).	% Third col
winning([[P,P,P],[_,_,_],[_,_,_]],win(P)) :- not(P = n).	% First row
winning([[_,_,_],[P,P,P],[_,_,_]],win(P)) :- not(P = n).	% Second row
winning([[_,_,_],[_,_,_],[P,P,P]],win(P)) :- not(P = n).	% Third row
winning([[P,_,_],[_,P,_],[_,_,P]],win(P)) :- not(P = n).	% First diag
winning([[_,_,P],[_,P,_],[P,_,_]],win(P)) :- not(P = n).	% Second diag
winning(_,nothing).																				% No winners

% DOC - Check the list of possible winners and decretate the final result
% aggregateResults(+ResultsList,-Result)
% TESTS
% aggregateResults([nothing,win(o),win(o),nothing],win(o)).	-> Yes
% aggregateResults([nothing,win(x),win(x),nothing],win(x)).	-> Yes
% aggregateResults([nothing,win(o),win(x),nothing],even).		-> Yes
% aggregateResults([nothing,nothing],nothing).							-> Yes
aggregateResults([],Elem).																							% Base of equality
aggregateResults([Elem|Tail],Elem) :- aggregateResults(Tail,Elem),!.		% Recursion of equality (all win(o) or win(x) or nothing)
aggregateResults([nothing|Tail],Elem) :- aggregateResults(Tail,Elem),!.	% Recursion if one is nothing
aggregateResults(_,even).																								% Case not win(o) or win(x) or nothing, then it is even

% DOC - Get the result for that board
% result(+Board,-Result)
% TESTS
% result([[n,x,n],[o,x,x],[o,x,n]],win(x)).		-> Yes
% result([[o,n,n],[o,x,x],[o,x,n]],win(o)).		-> Yes
% result([[o,x,n],[o,x,x],[o,x,n]],even).			-> Yes
% result([[n,n,n],[o,x,x],[o,x,n]],nothing).	-> Yes
result(Table,Result) :- findall(R,winning(Table,R),L), aggregateResults(L,Result).

% DOC
% game(@Table,@Player,-Result,-TableList)
% TESTS
% game([[n,n,n],[n,n,n],[n,n,n]],o,R,G).	-> All the possible plays
game(Table,Player,Result,[Table]) :-								% If the table is already winning, than the game is terminated.
	result(Table,Result),
	not(Result=nothing), !.
game(Table,Player,Result,[Table|TableList]) :-		% If the player can move,
	next(Table,Player,_,NewTable),										% he moves
	opponent(Player,Opponent),												% he gives turn to his opponent
	game(NewTable,Opponent,Result,TableList).					% and lets the opponent play his turn.

% DOC - Correlate the player with his opponent
% opponent(?Player,?Opponent)
opponent(o,x).
opponent(x,o).
