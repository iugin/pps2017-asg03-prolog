package it.iugin.pps.asg03

import java.io.{File, FileInputStream}

import alice.tuprolog.{Prolog, SolveInfo, Term, Theory}

trait PrologEngine {

  protected def mkPrologEngine(theory: Theory): Term => Stream[SolveInfo] = {
    val engine = new Prolog
    engine.setTheory(theory)

    goal =>
      new Iterable[SolveInfo] {

        override def iterator = new Iterator[SolveInfo] {
          var solution: Option[SolveInfo] = Some(engine.solve(goal))

          override def hasNext = solution.isDefined &&
            (solution.get.isSuccess || solution.get.hasOpenAlternatives)

          override def next() =
            try solution.get
            finally solution = if (solution.get.hasOpenAlternatives) Some(engine.solveNext()) else None
        }
      }.toStream
  }

  implicit def fileToTheory(f: File): Theory = new Theory(new FileInputStream(f))

  implicit def stringToTerm(s: String): Term = Term.createTerm(s)

  implicit def seqToTerm[T](s: Seq[T]): Term = s.mkString("[", ",", "]")

  implicit def stringToTheory[T](s: String): Theory = new Theory(s)

  implicit class RichSolution(solution: Stream[SolveInfo]) {

    def isSuccessful: Boolean = solution.map(_.isSuccess).headOption.contains(true)

    def hasSuccessfulSolutions(n: Int, recursiveSolution: Stream[SolveInfo] = solution): Boolean =
      if (n > 0)
      recursiveSolution.map(_.isSuccess).headOption.contains(true) &&
        hasSuccessfulSolutions(n-1, recursiveSolution.tail)
      else true
  }
}
